"""
URL configuration for marksheetpro project.

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/5.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path
from marksheetapp.views import AddStudentView, AllStudentsView, AddStudentClassView, AddSubjectView, GetStudentsView

urlpatterns = [
    path('admin/', admin.site.urls),
    path('add_class', AddStudentClassView.as_view()),
    path('add_subject', AddSubjectView.as_view()),
    path('add_student/', AddStudentView.as_view(), name='add_student'), 
    path('all_student/', AllStudentsView.as_view(), name='marksheet'),
     path('getstudents/', GetStudentsView.as_view(), name='get-students'),
    
]
