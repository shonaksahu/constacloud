from rest_framework import serializers
from .models import StudentClass, Subject, Student, Score

class StudentClassSerializer(serializers.ModelSerializer):
    class Meta:
        model = StudentClass
        fields = ['id', 'level']

class SubjectSerializer(serializers.ModelSerializer):
    class Meta:
        model = Subject
        fields = ['id', 'name', 'class_levels']

class ScoreSerializer(serializers.ModelSerializer):
    subject_name = serializers.CharField(source='subject.name', read_only=True)

    class Meta:
        model = Score
        fields = ['subject_name', 'score']

class StudentSerializer(serializers.ModelSerializer):
    subjects_scores = ScoreSerializer(many=True, read_only=True, source='score_set')
    total_score = serializers.ReadOnlyField()

    class Meta:
        model = Student
        fields = ['name', 'roll_no', 'class_level', 'subjects_scores', 'image', 'total_score']

    