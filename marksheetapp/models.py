
from django.core.validators import MinValueValidator, MaxValueValidator
from django.db import models

class StudentClass(models.Model):
    level = models.IntegerField(unique=True, validators=[MinValueValidator(1), MaxValueValidator(12)])

    def __str__(self):
        return str(self.level)

class Subject(models.Model):
    name = models.CharField(max_length=255)
    class_levels = models.ManyToManyField(StudentClass, related_name='subjects')

    def __str__(self):
        return self.name

class Student(models.Model):
    name = models.CharField(max_length=255)
    roll_no = models.CharField(max_length=20, unique=True)
    class_level = models.ForeignKey(StudentClass, on_delete=models.CASCADE)
    subjects = models.ManyToManyField(Subject, through='Score')
    image = models.ImageField(upload_to='student_images/', null=True, blank=True)

    def __str__(self):
        return self.name

class Score(models.Model):
    student = models.ForeignKey(Student, on_delete=models.CASCADE)
    subject = models.ForeignKey(Subject, on_delete=models.CASCADE)
    score = models.IntegerField(null=True, blank=True, default=0        )

    def __str__(self):
        return f"{self.student.name} - {self.subject.name}: {self.score}"
