from django.contrib import admin
from django.contrib import admin
from .models import StudentClass, Subject, Student, Score

# Register your models here.
admin.site.register(StudentClass)
admin.site.register(Subject)
admin.site.register(Student)
admin.site.register(Score)
