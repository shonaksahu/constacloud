
from django import template

register = template.Library()

@register.filter
def get_score(scores_dict, student_id, subject_id):
    return scores_dict.get(student_id, {}).get(subject_id)
