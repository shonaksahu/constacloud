from django.shortcuts import render, redirect
from django.views import View
from rest_framework import generics
from .forms import StudentForm, SubjectForm, StudentFormSet
from .models import Student, Subject, Score
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from .serializers import StudentClassSerializer, SubjectSerializer, StudentSerializer, ScoreSerializer
from rest_framework.response import Response
from rest_framework import status
from rest_framework.permissions import IsAuthenticated
from django.db.models import Sum, Count

class AddStudentClassView(generics.CreateAPIView):
    serializer_class = StudentClassSerializer

    def create(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        self.perform_create(serializer)
        headers = self.get_success_headers(serializer.data)
        return Response(serializer.data, status=status.HTTP_201_CREATED, headers=headers)
    
class AddSubjectView(generics.CreateAPIView):
    serializer_class = SubjectSerializer

    def create(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        self.perform_create(serializer)
        headers = self.get_success_headers(serializer.data)
        return Response(serializer.data, status=status.HTTP_201_CREATED, headers=headers)

class AddStudentView(View):
    template_name = 'add_student.html'

    def get(self, request):
        student_form = StudentForm()
        subject_form = SubjectForm()
        score_formset = StudentFormSet(prefix='scores')
        return render(request, self.template_name, {
            'student_form': student_form,
            'subject_form': subject_form,
            'score_formset': score_formset,
        })

    def post(self, request):
        student_form = StudentForm(request.POST, request.FILES)
        subject_form = SubjectForm(request.POST)
        score_formset = StudentFormSet(request.POST, prefix='scores')

        if student_form.is_valid() and subject_form.is_valid() and all(form.is_valid() for form in score_formset):
            student = student_form.save(commit=False)
            student.save()

            class_level = student.class_level
            selected_subjects = subject_form.cleaned_data.get('subjects', [])
            selected_scores = [(form.cleaned_data['subject'], form.cleaned_data['score']) for form in score_formset]

            student.subjects.clear()

            for subject in set(selected_subjects):
                student.subjects.add(subject)

            for subject, score_value in selected_scores:
                Score.objects.create(student=student, subject=subject, score=score_value)

            return redirect('add_student')

        return render(request, self.template_name, {
            'student_form': student_form,
            'subject_form': subject_form,
            'score_formset': score_formset,
        })
    
class AllStudentsView(View):
    template_name = 'all_students.html'
    items_per_page = 10

    def get(self, request):
        sort_by = request.GET.get('sort_by', 'name')
        sortable_columns = {'name', 'roll_no', 'total_score'}

        if sort_by not in sortable_columns:
            sort_by = 'name'
        students = Student.objects.annotate(total_score=Count('score')).order_by(sort_by)
        paginator = Paginator(students, self.items_per_page)
        page = request.GET.get('page', 1)

        try:
            students_paginated = paginator.page(page)
        except PageNotAnInteger:
            students_paginated = paginator.page(1)
        except EmptyPage:
            students_paginated = paginator.page(paginator.num_pages)
        return render(request, self.template_name, {'students': students_paginated, 'sort_by': sort_by})
    
class GetStudentsView(generics.ListAPIView):
    permission_classes = [IsAuthenticated]
    serializer_class = StudentSerializer

    def get_queryset(self):
        queryset = Student.objects.annotate(
            total_score=Sum('score__score')
        ).order_by('-total_score')

        class_level = self.request.query_params.get('class')
        if class_level:
            queryset = queryset.filter(class_level__level=class_level)

        return queryset
