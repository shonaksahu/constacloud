from django.apps import AppConfig


class MarksheetappConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'marksheetapp'
