
from django import forms
from .models import Student, Subject, Score

class StudentForm(forms.ModelForm):
    class Meta:
        model = Student
        fields = ['name', 'roll_no', 'class_level', 'image']

class SubjectForm(forms.ModelForm):
    class Meta:
        model = Subject
        fields = ['name']

class ScoreForm(forms.ModelForm):
    class Meta:
        model = Score
        fields = ['subject', 'score']
    
StudentFormSet = forms.formset_factory(ScoreForm, extra=5)
